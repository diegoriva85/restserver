## Aplicación del Servidor REST en Node con Swagger

### instalacion:
ejecutar ```npm install``` para las librerías:
"body-parser": "Analza los cuerpos de solicitud entrantes en el middleware antes de sus manejadores" 
https://www.npmjs.com/package/body-parser

"express": 
"Framework web rápido y minimalista para Nodejs" 
https://www.npmjs.com/package/express

"mongoose":
"Mongoose es una herramienta de modelado de objetos MongoDB diseñada para trabajar en un entorno asíncrono"
https://www.npmjs.com/package/mongoose

"mongoose-unique-validator":
"mongoose-unique-validator es un complemento que agrega validación previa al guardado para campos únicos dentro de un esquema Mongoose"
https://www.npmjs.com/package/mongoose-unique-validator

"swagger-ui-express": 
"Este módulo le permite servir documentos API generados automáticamente por swagger-ui desde express, basados ​​en un archivo swagger.json",
https://www.npmjs.com/package/swagger-ui-express

### Ejemplo de ejecucion:
```node server/serser.js```


### Definición de carpetas:
/server: "carpeta raiz del proyecto backend"

/server/config : "configuracion de la base de datos, puertos del servidor web y conexión d ela base de datos"

/server/models : "modelos de los bases de datos para su consumo"

/server/routes : "archivos de los servicios expuestos"

/db: "archivos para reconstruir la base sure y las dos colleciones clients y policies "

### Definición de archivos:

"/server/request.rest": "un archivo con ejemplos de cosnumo de los servicios que junto a extencion rest-client de visual studio code se pueden consumir los mismo" 

