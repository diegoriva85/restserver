const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let ValidRoles = {
    values: ['admin', 'user'],
    message: '{VALUE} invalid user rol'
};

let Schema = mongoose.Schema;

let clientsSchema = new Schema({
    name: {
        type: String,
        required: [true, 'field name is required']
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'email is required']
    },
    role: {
        type: String,
        default: 'user',
        enum: ValidRoles
    },
    // status: {
    //     type: Boolean,
    //     default: true
    // },
});


// userSchema.methods.toJSON = function(){

//     let user = this;
//     let userObject = user.toObject();
//     delete userObject.password;

//     return userObject;
// }


clientsSchema.plugin(uniqueValidator, { message: '{PATH} must be unique' });


module.exports = mongoose.model('clients', clientsSchema);