const mongoose = require('mongoose');
var f = new Date();

let Schema = mongoose.Schema;


let policiesSchema = new Schema({
    amountInsured: {
        type: Number,
        required: [true, 'field name is required']
    },
    email: {
        type: String,
        required: [true, 'email is required']
    },
    inceptionDate: {
        type: Date,
        default: f.getDate(),//verificr como capturar mejor la fecha
        required: [true, 'field inceptionDate is required']
    },
    installmentPayment:{
        type: Boolean,
        default: false
    },
    clientId: {
        type: String,
        required: [true, 'field clientId is required']
    },
    status: {
        type: Boolean,
        default: true
    },
});


module.exports = mongoose.model('policies', policiesSchema);