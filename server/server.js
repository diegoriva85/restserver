require('./config/config');
const express = require('express');
const mongoose = require('mongoose');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./config/swagger.json');


const app = express();

const bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

//Config global of routes
app.use(require('./routes/services'));

//conect to DB Mongo
mongoose.connect(process.env.URLDB,
    { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true},
     (err, res) => {

    if (err) throw err;

    console.log('Data base ONLINE');

});
//public de server in express
app.listen(process.env.PORT, () => {
    app.use('/', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    console.log('listening port: ', process.env.PORT);
});
