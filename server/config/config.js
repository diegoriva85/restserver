// ============================
//  Puerto / Port
// ============================
process.env.PORT = process.env.PORT || 3000;

// ============================
//  Entorno / Enviroment
// ============================
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';


// ============================
//  Base de datos
// ============================
let urlDB;

if (process.env.NODE_ENV === 'dev') {
   // urlDB = process.env.MONGO_URI;
    urlDB = 'mongodb+srv://consultas:consultas@cluster0-wv9ii.mongodb.net/sure'
} else {
    urlDB = 'mongodb://localhost:27017/sure';
}

process.env.URLDB = urlDB;