const express = require('express');
const _ = require('underscore');

//models of the tables in mongo
const Clients = require('../models/client_db');
const Policies = require('../models/policy_db');

const app = express();

/**
  * @desc public a service of clients
  * @param string $id - ID of the client that we want to match
  * @param string $name - Name of the client that we want to match
  * @param string $from - from of the query
  * @param string $limit - limit of the query
  * @return an array of objects with details of clients
*/
app.get('/clients', function (req, res) {

    let searchBy = {};
    let from = Number(req.query.from || 0 );
    let limit = Number(req.query.limit || 1000);
   
    ((req.query.id) && (searchBy._id = req.query.id)); 
    ((req.query.name) && (searchBy.name = req.query.name)); 
  
    Clients.find(searchBy, 'name email role')
        .skip(from)
        .limit(limit)
        .exec((err, clients) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }
            Clients.countDocuments(searchBy, (err, count) => {
                res.json({
                    ok: true,
                    clients,
                    total_rows: count
                });
            });
        });
});

/**
  * @desc public a service of pilicies
  * @param string $user - the email of the user that are making the query
  * @param string $email - the email of the policies that want to find
  * @param string $clientid - th clientid of the policies that want to find
  * @param string $from - from of the query
  * @param string $limit - limit of the query
  * @return an array of objects with details of policies
*/
app.get('/policies', function (req, res) {

    let searchBy = {};
    let from = Number(req.query.from || 0);
    let limit = Number(req.query.limit || 1000);

    ((req.query.email) && (searchBy.email = req.query.email));
    ((req.query.clientid) && (searchBy.clientId = req.query.clientid));

    if (!req.query.user) {
        return res.status(200).json({
            ok: false,
            err: {
                message: 'parameter user is required'
            }
        });
    }
    Clients.findOne({ email: req.query.user},)
        .exec((err, userdb) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }
            if (userdb.role === 'admin')
            {

                Policies.find(searchBy, 'amountInsured email inceptionDate installmentPayment clientId')
                    .skip(from)
                    .limit(limit)
                    .exec((err, policies) => {

                        if (err) {
                            return res.status(400).json({
                                ok: false,
                                err
                            });
                        }
                        Policies.countDocuments(searchBy, (err, count) => {

                            res.json({
                                ok: true,
                                policies,
                                total_rows: count
                            });
                        });
                    });
            }else{
                return res.status(200).json({
                    ok: false,
                    err: {
                        message: 'invalid user role'
                    }
                });
            }
       });

});



module.exports = app;